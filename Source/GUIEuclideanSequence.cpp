
#include "GUIEuclideanSequence.h"
#include "GUILookAndFeel.h"

#include <cmath>

namespace EuclideanRhythms {

  GUIEuclideanSequence::GUIEuclideanSequence(int idx, MessageQueue &msgQueueGuiToAudio) :
      index(idx), msgGuiToAudio(msgQueueGuiToAudio), beat(0), rhythmLengthTemp(0)
  {
    // calculateRhythm(steps, pulses, rotate, rhythm);

    centerX = Sizes::rhythmPadding + Sizes::rhythmControlsWidth + Sizes::rhythmDiameter / 2;
    centerY = Sizes::rhythmPadding + Sizes::rhythmDiameter / 2;

    labelSteps.setText("[ steps ]", juce::dontSendNotification);
    addAndMakeVisible(labelSteps);

    sliderSteps.setSliderStyle(juce::Slider::IncDecButtons);
    sliderSteps.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_AutoDirection);
    sliderSteps.setRange(1, 32, 1);
    sliderSteps.setValue(8);
    sliderSteps.onValueChange = [this] { this->stepsSliderChanged(); };
    addAndMakeVisible(sliderSteps);

    labelPulses.setText("( pulses )", juce::dontSendNotification);
    addAndMakeVisible(labelPulses);

    sliderPulses.setSliderStyle(juce::Slider::IncDecButtons);
    sliderPulses.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_AutoDirection);
    sliderPulses.setRange(0, 8, 1);
    sliderPulses.setValue(0);
    sliderPulses.onValueChange = [this] { this->pulsesSliderChanged(); };
    addAndMakeVisible(sliderPulses);

    labelRotate.setText("< rotate >", juce::dontSendNotification);
    addAndMakeVisible(labelRotate);

    sliderRotate.setSliderStyle(juce::Slider::IncDecButtons);
    sliderRotate.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_AutoDirection);
    sliderRotate.setRange(0, 8, 1);
    sliderRotate.setValue(0);
    sliderRotate.onValueChange = [this] { this->rotateSliderChanged(); };
    addAndMakeVisible(sliderRotate);
  }

  GUIEuclideanSequence::~GUIEuclideanSequence() {}

  void GUIEuclideanSequence::resized()
  {
    float x = Sizes::rhythmPadding / 2.0;
    float incrementY = Sizes::sliderHeight + Sizes::margin / 2.0;
    float verticalOffset = (getHeight() - (3 * incrementY)) / 2.0;
    float y = Sizes::rhythmPadding / 2.0 + verticalOffset;

    labelSteps.setBounds(x, y, Sizes::labelWidth, Sizes::sliderHeight);
    sliderSteps.setBounds(x + Sizes::labelWidth, y, Sizes::sliderWidth, Sizes::sliderHeight);

    y += incrementY;
    labelPulses.setBounds(x, y, Sizes::labelWidth, Sizes::sliderHeight);
    sliderPulses.setBounds(x + Sizes::labelWidth, y, Sizes::sliderWidth, Sizes::sliderHeight);

    y += incrementY;
    labelRotate.setBounds(x, y, Sizes::labelWidth, Sizes::sliderHeight);
    sliderRotate.setBounds(x + Sizes::labelWidth, y, Sizes::sliderWidth, Sizes::sliderHeight);

    calculatePositions(rhythm.notes.size(), centerX, centerY, squarePositions,
                       squareCenterPositions);
  }

  void GUIEuclideanSequence::stepsSliderChanged()
  {
    int value = sliderSteps.getValue();
    msgGuiToAudio.sendInt(MessageType::Steps, index, value);
    sliderPulses.setRange(0, value, 1);
    sliderRotate.setRange(0, value, 1);
  }

  void GUIEuclideanSequence::rotateSliderChanged()
  {
    int value = sliderRotate.getValue();
    msgGuiToAudio.sendInt(MessageType::Rotate, index, value);
  }

  void GUIEuclideanSequence::pulsesSliderChanged()
  {
    int value = sliderPulses.getValue();
    msgGuiToAudio.sendInt(MessageType::Pulses, index, value);
  }

  void GUIEuclideanSequence::setBeat(int b)
  {
    beat = b;
  }

  void GUIEuclideanSequence::setRhythm(unsigned int r)
  {
    rhythm.fromInt(r, rhythmLengthTemp);
    calculatePositions(rhythm.notes.size(), centerX, centerY, squarePositions,
                       squareCenterPositions);
  }

  void GUIEuclideanSequence::setRhythmLength(int l)
  {
    rhythmLengthTemp = l;
  }

  void GUIEuclideanSequence::paint(juce::Graphics &g)
  {
    g.fillAll(Colours::background);
    drawCircle(g, centerX, centerY, beat);
    g.setColour(Colours::soft);
    g.drawRect(getLocalBounds());
  }

  void GUIEuclideanSequence::calculatePositions(int steps, float centerX, float centerY,
                                                juce::Array<juce::Point<float>> &positions,
                                                juce::Array<juce::Point<float>> &centerPositions)
  {

    positions.clearQuick();
    centerPositions.clearQuick();

    // Dimensions for main circle
    float diameter = Sizes::rhythmDiameter;
    float radius = diameter / 2.0;

    // Steps are drawn as squares along the perimeter of the circle.
    float squareSize = Sizes::rhythmStep;
    float twoPi = juce::MathConstants<float>::twoPi;
    float angleIncrement = twoPi / steps;

    for (int i = 0; i < steps; ++i) {
      float angle = i * angleIncrement;

      // center of the square
      float xx = centerX + radius * std::sin(angle);
      float yy = centerY - radius * std::cos(angle);

      // top left of the square
      float x = std::floor(xx - squareSize / 2.0);
      float y = std::floor(yy - squareSize / 2.0);

      positions.add(juce::Point<float>(x, y));
      centerPositions.add(juce::Point<float>(xx, yy));
    }
  }

  void GUIEuclideanSequence::drawCircle(juce::Graphics &g, float centerX, float centerY,
                                        int currentStep)
  {

    // Dimensions for main circle
    float diameter = Sizes::rhythmDiameter;
    float radius = diameter / 2.0;
    g.setColour(Colours::soft);
    float leftX = centerX - radius;
    float topY = centerY - radius;
    g.drawEllipse(leftX, topY, diameter, diameter, Sizes::lineThickness);

    // Steps are drawn as squares along the perimeter of the circle.
    int nSteps = rhythm.notes.size();
    int nPulses = 0;
    juce::Point<float> previousPulse = {0, 0};
    juce::Point<float> firstPulse = {0, 0};

    for (int i = 0; i < nSteps; ++i) {
      bool isCurrentBeat = (currentStep % rhythm.notes.size()) == i;
      if (isCurrentBeat) {
        g.setColour(Colours::highlight);
      }
      else {
        g.setColour(Colours::hard);
      }
      float xx = squareCenterPositions[i].getX();
      float yy = squareCenterPositions[i].getY();
      float x = squarePositions[i].getX();
      float y = squarePositions[i].getY();
      float squareSize = Sizes::rhythmStep;
      g.drawRect(x, y, squareSize, squareSize, Sizes::lineThickness);

      // Should this step be enabled?
      bool enabled = rhythm.notes[i];
      if (enabled) {
        g.fillRect(x, y, squareSize, squareSize);

        if (nPulses != 0) {
          g.setColour(Colours::hard);
          g.drawLine(previousPulse.x, previousPulse.y, xx, yy, Sizes::lineThickness * 2);
        }
        else {
          firstPulse.x = xx;
          firstPulse.y = yy;
        }
        nPulses++;
        previousPulse.x = xx;
        previousPulse.y = yy;
      }
    }

    if (nPulses > 2) {
      g.setColour(Colours::hard);
      g.drawLine(previousPulse.x, previousPulse.y, firstPulse.x, firstPulse.y,
                 Sizes::lineThickness * 2);
    }
  }
}
