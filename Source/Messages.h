#pragma once

#include "readerwriterqueue.h"

namespace EuclideanRhythms {

// clang-format off
  #define MESSAGE_QUEUE_SIZE 64
// clang-format on

enum class MessageType {
  Play,
  Beat,
  Tempo,
  Steps,
  Pulses,
  Rotate,
  Rhythm,
  RhythmLength,
  NewSequence,
  DeleteSequence
};

union MessageValue {
  int integer;
  float floatingPoint;
  unsigned int unsignedInteger;
};

class Message
{
 public:
  MessageType type;
  MessageValue value;
  int index;

  int getIntValue();
  unsigned int getUIntValue();
  float getFloatValue();
};

class MessageQueue : public moodycamel::ReaderWriterQueue<Message>
{
 private:
  Message outgoingMessage;

 public:
  MessageQueue();

  bool sendFloat(MessageType type, float value);
  bool sendFloat(MessageType type, int index, float value);
  bool sendInt(MessageType type, int value);
  bool sendInt(MessageType type, int index, int value);
  bool sendUInt(MessageType type, unsigned int value);
  bool sendUInt(MessageType type, int index, unsigned int value);
  bool receive(Message &msg);
};
}
