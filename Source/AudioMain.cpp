#include "AudioMain.h"

namespace EuclideanRhythms {

/////////////////////////////////////////////////////////////////////////////
// Constructor, initialization...

AudioMain::AudioMain(MessageQueue &msgGuiToAudio, MessageQueue &msgAudioToGui) :
    msgGuiToAudio(msgGuiToAudio),
    msgAudioToGui(msgAudioToGui),
    previousTime(0),
    tempo(0),
    tempoMillis(0),
    halfTempoMillis(0),
    beatCounter(0),
    beatCounterRemainder(0),
    playing(false),
    maxSequences(32)
{
  juce::MidiDeviceInfo midiIn = juce::MidiInput::getDefaultDevice();
  audioManager.setMidiInputDeviceEnabled(midiIn.identifier, true);
  audioManager.addMidiInputDeviceCallback(midiIn.identifier, this);

  juce::MidiDeviceInfo midiOut = juce::MidiOutput::getDefaultDevice();
  audioManager.setDefaultMidiOutputDevice(midiOut.identifier);

  for (int i = 0; i < maxSequences; ++i) {
    AudioEuclideanSequence *seq = new AudioEuclideanSequence(i, this);
    sequences.add(seq);
  }

  startTimer(10);
}

AudioMain::~AudioMain() {}

juce::AudioDeviceManager &AudioMain::getAudioManager()
{
  return audioManager;
}

MessageQueue &AudioMain::getAudioToGuiMsgQueue()
{
  return msgAudioToGui;
}

/////////////////////////////////////////////////////////////////////////////
// Timer

void AudioMain::hiResTimerCallback()
{
  handleMessages();
  tick();
}

void AudioMain::tick()
{
  if (!playing) {
    return;
  }

  double currentTime = juce::Time::getMillisecondCounterHiRes();
  double elapsedTime = currentTime - previousTime;
  if (elapsedTime < halfTempoMillis) {
    return;
  }
  else if (beatCounterRemainder == 0 && elapsedTime >= halfTempoMillis &&
           elapsedTime < tempoMillis) {
    for (auto sequence : sequences) {
      if (sequence->isActive()) {
        sequence->tick(beatCounter, beatCounterRemainder);
      }
    }
    msgAudioToGui.sendInt(MessageType::Beat, beatCounter);
    beatCounterRemainder = 1;
  }
  else if (elapsedTime >= tempoMillis) {
    for (auto sequence : sequences) {
      if (sequence->isActive()) {
        sequence->tick(beatCounter, beatCounterRemainder);
      }
    }
    msgAudioToGui.sendInt(MessageType::Beat, beatCounter);
    previousTime = currentTime;
    beatCounter++;
    beatCounterRemainder = 0;
  }
}

//////////////////////////////////////////////////////////////////////////////
// Incoming Messages from GUI.

void AudioMain::handleMessages()
{
  while (msgGuiToAudio.receive(incomingMessage)) {
    switch (incomingMessage.type) {
      case MessageType::Tempo:
        setTempo(incomingMessage.getFloatValue());
        break;

      case MessageType::Play:
        setPlaying(incomingMessage.getIntValue());
        break;

      case MessageType::Steps:
        setSteps(incomingMessage.index, incomingMessage.getIntValue());
        break;

      case MessageType::Pulses:
        setPulses(incomingMessage.index, incomingMessage.getIntValue());
        break;

      case MessageType::Rotate:
        setRotation(incomingMessage.index, incomingMessage.getIntValue());
        break;

      case MessageType::NewSequence:
        newSequence(incomingMessage.getIntValue());
        break;

      case MessageType::DeleteSequence:
        deleteSequence(incomingMessage.getIntValue());
        break;

      default:
      case MessageType::Beat:
      case MessageType::Rhythm:
      case MessageType::RhythmLength:
        break;
    }
  }
}

void AudioMain::newSequence(int i)
{
  auto seq = sequences[i];
  jassert(!seq->isActive());
  seq->setActive(true);
}

void AudioMain::deleteSequence(int i)
{
  auto seq = sequences[i];
  jassert(seq->isActive());
  seq->setActive(false);
}

void AudioMain::setSteps(int index, int steps)
{
  auto seq = sequences[index];
  if (seq->isActive()) {
    seq->setSteps(steps);
  }
}

void AudioMain::setPulses(int index, int pulses)
{
  auto seq = sequences[index];
  if (seq->isActive()) {
    seq->setPulses(pulses);
  }
}

void AudioMain::setRotation(int index, int rotation)
{
  auto seq = sequences[index];
  if (seq->isActive()) {
    seq->setRotation(rotation);
  }
}

void AudioMain::setTempo(float t)
{
  tempo = t;
  tempoMillis = 60000.0 / t;
  halfTempoMillis = tempoMillis / 2.0;
}

void AudioMain::setPlaying(int p)
{
  playing = p;
  if (!playing) {
    beatCounter = 0;
    beatCounterRemainder = 0;
    msgAudioToGui.sendInt(MessageType::Beat, 0);

    for (auto sequence : sequences) {
      if (sequence->isActive()) {
        sequence->stop();
      }
    }
  }
  else {
    // Here, we need to fake some timer events by calling tick() directly so that the notes at time
    // 0 (right after clicking play button) are fired.
    double currentTime = juce::Time::getMillisecondCounterHiRes();
    previousTime = currentTime - tempoMillis;
    tick();
    previousTime = currentTime - halfTempoMillis;
    beatCounterRemainder = 1;
    beatCounter = 0;
  }
}

//////////////////////////////////////////////////////////////////////////////
// MIDI

void AudioMain::handleIncomingMidiMessage(juce::MidiInput * /* source */,
                                          const juce::MidiMessage &message)
{
  processMidi(message);
}

void AudioMain::processMidi(const juce::MidiMessage & /* message */)
{
  // // Debug
  // int n = message.getRawDataSize();
  // const unsigned char *data = message.getRawData();
  // juce::String msg = "";
  // for (int i = 0; i < n; ++i) {
  //   msg << data[i] << " ";
  // }
  // DBG(msg);
}

} // namespace EuclideanRhythms
