#include "GUILookAndFeel.h"
#include "BinaryData.h"

namespace EuclideanRhythms {

GUILookAndFeel::GUILookAndFeel()
{
  // Background colours for buttons
  setColour(juce::TextButton::buttonColourId, Colours::background);
  setColour(juce::TextButton::buttonOnColourId, Colours::background);

  // Outline colours for buttons, etc
  setColour(juce::ComboBox::outlineColourId, Colours::hard);
  setColour(juce::Slider::textBoxOutlineColourId, Colours::hard);
  setColour(juce::TextEditor::outlineColourId, Colours::hard);
  setColour(juce::TextEditor::focusedOutlineColourId, Colours::hard);

  // Scrollbar colours
  setColour(juce::ScrollBar::thumbColourId, Colours::soft);

  // Load fonts
  auto tfl =
      juce::Typeface::createSystemTypefaceFor(BinaryData::Light_ttf, BinaryData::Light_ttfSize);
  fontLight = juce::Font(tfl);
  fontLight = fontLight.withPointHeight(14);

  // auto tfm =
  //     juce::Typeface::createSystemTypefaceFor(BinaryData::Regular_ttf,
  //     BinaryData::Regular_ttfSize);
  // fontMedium = juce::Font(tfm);
  // fontMedium = fontMedium.withPointHeight(16);

  // auto tfb =
  //     juce::Typeface::createSystemTypefaceFor(BinaryData::Bold_ttf, BinaryData::Bold_ttfSize);
  // fontBold = juce::Font(tfb);
  // fontMedium = fontBold.withPointHeight(16);
}

GUILookAndFeel::~GUILookAndFeel() {}

// const juce::Font &GUILookAndFeel::getMediumFont()
// {
//   return fontMedium;
// }

// const juce::Font &GUILookAndFeel::getBoldFont()
// {
//   return fontBold;
// }

const juce::Font &GUILookAndFeel::getLightFont()
{
  return fontLight;
}

juce::Font GUILookAndFeel::getLabelFont(juce::Label & /*label*/)
{
  return getLightFont();
}

// Change the sizes of the textbox and the buttons for the incDecButtons
// sliders.
juce::Slider::SliderLayout GUILookAndFeel::getSliderLayout(juce::Slider &slider)
{
  auto area = slider.getLocalBounds();
  // float w = area.getWidth();
  float h = area.getHeight();
  juce::Slider::SliderLayout layout;
  layout.sliderBounds = area.removeFromRight(h / 2.0 + 5.0);
  layout.textBoxBounds = area;
  return layout;
}

// Overriding how the button outline and backgrounds are drawn.
// Just a simple rect.
void GUILookAndFeel::drawButtonBackground(juce::Graphics &g, juce::Button &button,
                                          const juce::Colour &backgroundColour,
                                          bool /* shouldDrawButtonAsHighlighted */,
                                          bool /* shouldDrawButtonAsDown */)
{
  auto bounds = button.getLocalBounds().toFloat();
  g.setColour(backgroundColour);
  g.fillRect(bounds);
  g.setColour(button.findColour(juce::ComboBox::outlineColourId));
  g.drawRect(bounds);
}

} // namespace EuclideanRhythms
