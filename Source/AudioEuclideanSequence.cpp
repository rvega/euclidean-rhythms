#include "AudioEuclideanSequence.h"
#include "AudioMain.h"

namespace EuclideanRhythms {

AudioEuclideanSequence::AudioEuclideanSequence(int idx, AudioMain *audioMain) :
    audioMain(*audioMain),
    index(idx),
    active(false),
    steps(8),
    pulses(0),
    rotation(0),
    midiChannel(1),
    midiNote(64),
    isNoteOn(false)
{
}

AudioEuclideanSequence::~AudioEuclideanSequence() {}

void AudioEuclideanSequence::setActive(bool a)
{
  active = a;
}

bool AudioEuclideanSequence::isActive()
{
  return active;
}

void AudioEuclideanSequence::tick(int currentBeat, int remainder)
{
  jassert(active);

  if (isNoteOn) {
    noteOff();
  }

  bool note = rhythm.notes[currentBeat % rhythm.notes.size()];
  if (remainder == 0 && note) {
    noteOn();
  }
}

void AudioEuclideanSequence::stop()
{
  if (isNoteOn) {
    noteOff();
  }
}

void AudioEuclideanSequence::noteOn()
{
  juce::MidiOutput *midiOutput = audioMain.getAudioManager().getDefaultMidiOutput();
  if (midiOutput == nullptr) {
    return;
  }

  juce::uint8 velocity = 65;
  auto noteOn = juce::MidiMessage::noteOn(midiChannel, midiNote, velocity);
  midiOutput->sendMessageNow(noteOn);
  isNoteOn = true;
}

void AudioEuclideanSequence::noteOff()
{
  juce::MidiOutput *midiOutput = audioMain.getAudioManager().getDefaultMidiOutput();
  if (midiOutput == nullptr) {
    return;
  }

  juce::uint8 velocity = 65;
  auto noteOff = juce::MidiMessage::noteOff(midiChannel, midiNote, velocity);
  midiOutput->sendMessageNow(noteOff);
  isNoteOn = false;
}

void AudioEuclideanSequence::setSteps(int s)
{
  steps = s;
  rhythm.calculate(steps, pulses, rotation);

  unsigned int rhythmInt;
  int length;
  rhythm.toInt(rhythmInt, length);
  audioMain.getAudioToGuiMsgQueue().sendInt(MessageType::RhythmLength, index, length);
  audioMain.getAudioToGuiMsgQueue().sendUInt(MessageType::Rhythm, index, rhythmInt);
}

void AudioEuclideanSequence::setPulses(int p)
{
  pulses = p;
  rhythm.calculate(steps, pulses, rotation);

  unsigned int rhythmInt;
  int length;
  rhythm.toInt(rhythmInt, length);
  audioMain.getAudioToGuiMsgQueue().sendInt(MessageType::RhythmLength, index, length);
  audioMain.getAudioToGuiMsgQueue().sendUInt(MessageType::Rhythm, index, rhythmInt);
}

void AudioEuclideanSequence::setRotation(int r)
{
  rotation = r;
  rhythm.calculate(steps, pulses, rotation);

  unsigned int rhythmInt;
  int length;
  rhythm.toInt(rhythmInt, length);
  audioMain.getAudioToGuiMsgQueue().sendInt(MessageType::RhythmLength, index, length);
  audioMain.getAudioToGuiMsgQueue().sendUInt(MessageType::Rhythm, index, rhythmInt);
}

} // namespace EuclideanRhythms
