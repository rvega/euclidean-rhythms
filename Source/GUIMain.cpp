#include <cmath>

#include "BinaryData.h"
#include "GUILookAndFeel.h"
#include "GUIMain.h"

namespace EuclideanRhythms {

/////////////////////////////////////////////////////////////////////////////

SettingsComponent::SettingsComponent(juce::AudioDeviceManager &audioMgr) :
    doneButton("Done"), juceSettings(audioMgr, 0, 0, 0, 0, true, true, true, false)
{
  addAndMakeVisible(juceSettings);
  addAndMakeVisible(doneButton);
}

void SettingsComponent::resized()
{
  auto bounds = getLocalBounds().reduced(Sizes::margin);
  juceSettings.setBounds(bounds);

  int x = bounds.getWidth() - Sizes::buttonWidth - Sizes::margin;
  int y = bounds.getHeight() - Sizes::sliderHeight - Sizes::margin;
  int w = Sizes::buttonWidth;
  int h = Sizes::sliderHeight;
  doneButton.setBounds(x, y, w, h);
  doneButton.onClick = [this] { setVisible(false); };
}

void SettingsComponent::paint(juce::Graphics &g)
{
  g.fillAll(Colours::background);
  g.setColour(Colours::soft);
  g.drawRect(getLocalBounds().reduced(Sizes::margin), 1);
}

/////////////////////////////////////////////////////////////////////////////

TopRowComponent::TopRowComponent(GUIMain &guiMain) :
    guiMain(guiMain),
    playButton("Play", Colours::hard, Colours::hard, Colours::highlight),
    tempoButton("Tempo"),
    tempoSlider("TempoSlider"),
    tempoClickCounter(0),
    tempoClickTimes{0, 0},
    plusButton("Plus", Colours::hard, Colours::hard, Colours::highlight),
    minusButton("Minus", Colours::hard, Colours::hard, Colours::highlight)
{
  playButton.setOnColours(Colours::highlight, Colours::highlight, Colours::hard);
  playButton.shouldUseOnColours(true);
  playButton.setTriggeredOnMouseDown(true);
  juce::Path triangle;
  triangle.addTriangle(0, 0, 1.0, 0.5, 0, 1.0);
  playButton.setShape(triangle, false, false, false);
  playButton.onClick = [this] { this->play(); };
  addAndMakeVisible(playButton);

  const juce::Image metronomeImage =
      juce::ImageCache::getFromMemory(BinaryData::Metronome_png, BinaryData::Metronome_pngSize);
  tempoButton.setImages(false, true, true, metronomeImage, 1.0, {}, metronomeImage, 1.0, {},
                        metronomeImage, 1.0, Colours::highlight);
  tempoButton.setTriggeredOnMouseDown(true);
  addAndMakeVisible(tempoButton);

  int initialTempo = 60;
  tempoSlider.setSliderStyle(juce::Slider::IncDecButtons);
  tempoSlider.setIncDecButtonsMode(juce::Slider::incDecButtonsDraggable_AutoDirection);
  tempoSlider.setRange(20, 666, 1);
  tempoSlider.setValue(initialTempo);
  tempoSlider.onValueChange = [this] { this->tempoSliderChanged(); };
  addAndMakeVisible(tempoSlider);

  guiMain.msgGuiToAudio.sendFloat(MessageType::Tempo, initialTempo);

  ///////////////////////////

  plusButton.setOnColours(Colours::highlight, Colours::highlight, Colours::hard);
  plusButton.shouldUseOnColours(true);
  juce::Path plus;
  plus.addRectangle(0.0, 0.45, 1.0, 0.1);
  plus.addRectangle(0.45, 0.0, 0.1, 1.0);
  plusButton.setShape(plus, false, true, false);
  plusButton.onClick = [this] { this->guiMain.plusButtonClick(); };
  addAndMakeVisible(plusButton);

  minusButton.setOnColours(Colours::highlight, Colours::highlight, Colours::hard);
  minusButton.shouldUseOnColours(true);
  juce::Path minus;
  minus.addRectangle(0.0, 0.45, 1.0, 0.1);
  minusButton.setShape(minus, false, true, false);
  minusButton.onClick = [this] { this->guiMain.minusButtonClick(); };
  addAndMakeVisible(minusButton);

  ///////////////////////////

  const juce::Image gearImage =
      juce::ImageCache::getFromMemory(BinaryData::Settings_png, BinaryData::Settings_pngSize);
  settingsButton.setImages(false, true, true, gearImage, 1.0, {}, gearImage, 1.0, {}, gearImage,
                           1.0, Colours::highlight);
  settingsButton.onClick = [this] { this->guiMain.settingsComponent.setVisible(true); };
  addAndMakeVisible(settingsButton);
}

void TopRowComponent::paint(juce::Graphics &g)
{
  g.setColour(Colours::soft);

  // Tempo square
  auto tempoSquare = getLocalBounds();
  tempoSquare.setWidth(Sizes::transportWidth);
  g.drawRect(tempoSquare);

  // Plus minus square
  int x = Sizes::transportWidth + Sizes::margin / 2.0;
  int y = 0;
  int h = Sizes::sliderHeight + Sizes::margin;
  int w = 2.0 * Sizes::sliderHeight + 1.75 * Sizes::margin;
  g.drawRect(x, y, w, h);

  // Settings Btn square
  x = x + w + Sizes::margin / 2.0;
  w = Sizes::sliderHeight + Sizes::margin;
  g.drawRect(x, y, w, h);
}

void TopRowComponent::resized()
{
  float x = Sizes::margin * 0.5;
  float y = Sizes::margin * 0.5;
  float w = Sizes::sliderHeight;
  float h = Sizes::sliderHeight;
  playButton.setBounds(x, y, w, h);

  x = x + w + Sizes::margin;
  tempoButton.setBounds(x, y, w, h);
  tempoButton.onClick = [this] { this->tempoButtonClicked(); };

  x = x + w + Sizes::margin / 3.0;
  w = Sizes::sliderWidth * 1.2;
  tempoSlider.setBounds(x, y, w, h);

  x = x + w + 1.5 * Sizes::margin;
  w = Sizes::sliderHeight;
  plusButton.setBounds(x, y, w, h);

  x = x + w + 0.75 * Sizes::margin;
  minusButton.setBounds(x, y, w, h);

  x = x + w + 1.5 * Sizes::margin;
  settingsButton.setBounds(x, y, w, h);
}

void TopRowComponent::play()
{
  playing = !playing;
  playButton.setToggleState(playing, juce::dontSendNotification);
  guiMain.msgGuiToAudio.sendInt(MessageType::Play, playing);
}

void TopRowComponent::tempoSliderChanged()
{
  float value = tempoSlider.getValue();
  guiMain.msgGuiToAudio.sendFloat(MessageType::Tempo, value);
}

void TopRowComponent::tempoButtonClicked()
{
  int i = tempoClickCounter % 2;
  double previousTime = tempoClickTimes[i];
  double currentTime = juce::Time::getMillisecondCounterHiRes();

  // timeout?
  if (currentTime - previousTime > 3000) {
    tempoClickCounter = 0;
    tempoClickTimes[0] = 0;
    tempoClickTimes[1] = 0;
  }

  if (tempoClickCounter > 1) {
    double t0, t1;
    if (i == 1) {
      t0 = tempoClickTimes[1] - tempoClickTimes[0];
      t1 = currentTime - tempoClickTimes[1];
    }
    else {
      t0 = tempoClickTimes[0] - tempoClickTimes[1];
      t1 = currentTime - tempoClickTimes[0];
    }
    double t = (t0 + t1) / 2.0;
    double bpm = 60000.0 / t;

    tempoSlider.setValue(bpm, juce::dontSendNotification);
    guiMain.msgGuiToAudio.sendFloat(MessageType::Tempo, bpm);
  }

  tempoClickCounter++;
  i = tempoClickCounter % 2;
  tempoClickTimes[i] = currentTime;
}

/////////////////////////////////////////////////////////////////////////////

GUIMain::~GUIMain()
{
  setLookAndFeel(nullptr);
}

GUIMain::GUIMain(MessageQueue &msgGuiToAudio, MessageQueue &msgAudioToGui,
                 juce::AudioDeviceManager &audioMgr) :
    msgGuiToAudio(msgGuiToAudio),
    msgAudioToGui(msgAudioToGui),
    topRowComponent(*this),
    settingsComponent(audioMgr),
    maxSequences(32),
    initialSequences(2)
{
  setLookAndFeel(&guiLookAndFeel);

  // There are 3 objects in play for the layout and scrolling of the sequences (instances of
  // GUIEuclideanSequence). scrollBar (instance of juce::ViewPort) takes care of scrolling whatever
  // is in scrollContainer (instance of juce::Component) and scrollFlexBox (instance of
  // juce::FlexBox) takes care of setting the positions for the sequences

  // Create sequences, store them in the sequences array for easy handling afterwards and add them
  // to scrollContainer.
  for (int i = 0; i < initialSequences; ++i) {
    plusButtonClick();
  }

  // Initialize the scrollFlexBox so that it can properly lay out the sequences.
  scrollFlexBox.flexWrap = juce::FlexBox::Wrap::wrap;
  scrollFlexBox.justifyContent = juce::FlexBox::JustifyContent::flexStart;
  scrollFlexBox.alignContent = juce::FlexBox::AlignContent::flexStart;
  scrollFlexBox.alignItems = juce::FlexBox::AlignItems::flexStart;

  // Add the scrollContainer to the scrollBar and add the scrollBar to the mainGUI.
  scrollBar.setViewedComponent(&scrollContainer, false);
  scrollBar.setScrollBarsShown(true, false);
  addAndMakeVisible(scrollBar);

  ///////////////////////

  addAndMakeVisible(topRowComponent);
  addChildComponent(settingsComponent);

  setFramesPerSecond(12);

  // Keep this line last in this function. It will call GUIMain::resized().
  setSize(Sizes::windowWidth, Sizes::windowHeight);
}

void GUIMain::resized()
{
  juce::Rectangle<int> fullArea = getLocalBounds();

  juce::Rectangle<int> topRowBounds =
      fullArea.removeFromTop(Sizes::topRowHeight + 2.0 * Sizes::margin);
  topRowBounds.reduce(Sizes::margin, Sizes::margin);
  topRowComponent.setBounds(topRowBounds);

  scrollBar.setBounds(fullArea); // Full area has been reduced by removeFromTop()

  float scrollHeight = sequences.getLast()->getBottom() + Sizes::margin;
  float scrollWidth = scrollBar.getWidth();
  juce::Rectangle<int> scrollArea(0, 0, scrollWidth, scrollHeight);
  scrollContainer.setBounds(scrollArea);
  scrollFlexBox.performLayout(scrollArea);

  // Settings dialog, hidden by default
  settingsComponent.setBounds(getLocalBounds());
}

void GUIMain::paint(juce::Graphics &g)
{
  g.fillAll(Colours::background);
}

void GUIMain::plusButtonClick()
{
  if (sequences.size() >= maxSequences) {
    return;
  }

  // Tell the audio engine to add a new sequence.
  msgGuiToAudio.sendInt(MessageType::NewSequence, sequences.size());

  // Create the new GUISequence object and add it to the component hierarchy.
  GUIEuclideanSequence *seq = new GUIEuclideanSequence(sequences.size(), msgGuiToAudio);
  sequences.add(seq);
  scrollContainer.addAndMakeVisible(seq);

  // Add a new flexbox item and ...
  juce::FlexItem item(*seq);
  item.width = Sizes::rhythmWidth;
  item.height = Sizes::rhythmHeight;
  item.margin = juce::FlexItem::Margin(0.0, 0.0, Sizes::margin, Sizes::margin);
  scrollFlexBox.items.add(item);

  // Calling resized() twice. It's ugly but it's a lot easier than calculating scroll areas and
  // calling scrollFlexBox.performLayout() here. This is because we're using
  // sequences.getLast()->getBottom() to calculate the correct height for the scrollbar and we just
  // added a new sequence so getBottom() will return 0. Next time, after performLayout(), getBottom
  // will return the correct value.
  resized();
  resized();
}

void GUIMain::minusButtonClick()
{
  if (sequences.size() <= 1) {
    return;
  }

  // Tell the audio engine to remove last sequence.
  msgGuiToAudio.sendInt(MessageType::DeleteSequence, sequences.size() - 1);

  // Delete the GUISequence object and the flexbox item.
  GUIEuclideanSequence *seq = sequences.getLast();
  scrollContainer.removeChildComponent(seq);
  scrollFlexBox.items.removeLast();
  sequences.removeLast(); // will delete the GUIEuclideanSequence object.

  resized();
}

/////////////////////////////////////////////////////////////////////////////

void GUIMain::update()
{
  while (msgAudioToGui.receive(incomingMessage)) {
    if (incomingMessage.type == MessageType::Beat) {
      setBeat(incomingMessage.getIntValue());
    }
    else if (incomingMessage.type == MessageType::RhythmLength) {
      setRhythmLength(incomingMessage);
    }
    else if (incomingMessage.type == MessageType::Rhythm) {
      setRhythm(incomingMessage);
    }
  }
}

void GUIMain::setRhythmLength(Message &msg)
{
  int i = msg.index;
  sequences[i]->setRhythmLength(msg.getIntValue());
}

void GUIMain::setRhythm(Message &msg)
{
  int i = msg.index;
  sequences[i]->setRhythm(msg.getUIntValue());
}

void GUIMain::setBeat(int b)
{
  for (auto sequence : sequences) {
    sequence->setBeat(b);
  }
}

} // namespace EuclideanRhythms
