#pragma once

#include <juce_core/juce_core.h>

#include "Messages.h"
#include "Rhythm.h"

namespace EuclideanRhythms {

  class AudioMain;

  class AudioEuclideanSequence {
   public:
    AudioEuclideanSequence(int idx, AudioMain *audioMain);
    ~AudioEuclideanSequence();

    void setSteps(int s);
    void setPulses(int p);
    void setRotation(int r);

    bool isActive();
    void setActive(bool a);

    /**
     * @brief Remainder is either 0 (current time is the start of a beat) 
     *        or 1 (current time is half of a beat. Useful for note offs).
     */
    void tick(int currentBeat, int remainder);

    void stop();

   private:
    AudioMain &audioMain;
    int index;
    bool active;
    int steps;
    int pulses;
    int rotation;
    Rhythm rhythm;
    int midiChannel;
    int midiNote;
    bool isNoteOn;

    void noteOn();
    void noteOff();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioEuclideanSequence)
  };
}
