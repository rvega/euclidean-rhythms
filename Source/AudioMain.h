#pragma once

#include <juce_audio_devices/juce_audio_devices.h>

#include "AudioEuclideanSequence.h"
#include "Messages.h"

namespace EuclideanRhythms {
class AudioMain : public juce::MidiInputCallback, public juce::HighResolutionTimer
{
 public:
  AudioMain(MessageQueue &msgGuiToAudio, MessageQueue &msgAudioToGui);
  ~AudioMain();

  juce::AudioDeviceManager &getAudioManager();
  MessageQueue &getAudioToGuiMsgQueue();

  void handleIncomingMidiMessage(juce::MidiInput *source, const juce::MidiMessage &message);
  void hiResTimerCallback();

 private:
  juce::AudioDeviceManager audioManager;
  MessageQueue &msgGuiToAudio;
  MessageQueue &msgAudioToGui;
  Message incomingMessage;

  double previousTime;
  float tempo;
  float tempoMillis;
  float halfTempoMillis;
  unsigned long int beatCounter;
  int beatCounterRemainder;
  bool playing;
  void processMidi(const juce::MidiMessage &message);
  void newSequence(int i);
  void deleteSequence(int i);
  void setTempo(float t);
  void setPlaying(int p);
  void handleMessages();
  void tick();
  void setSteps(int index, int steps);
  void setPulses(int index, int pulses);
  void setRotation(int index, int rotation);

  const int maxSequences;
  juce::OwnedArray<AudioEuclideanSequence> sequences;

  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioMain)
};
}
