#pragma once

#include <juce_core/juce_core.h>

namespace EuclideanRhythms {

#define MAX_RHYTHM_SIZE 32

  class Rhythm {
   public:
    juce::Array<bool> notes;

    Rhythm();
    ~Rhythm();
    void calculate(int steps, int pulses, int offset);

    // We use an integer representation of a rhythm so it's cheap to pass it around in messages.
    void toInt(unsigned int &i, int &length);
    void fromInt(unsigned int i, int length);
  };

}
