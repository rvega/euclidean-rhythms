#pragma once

#include <juce_gui_extra/juce_gui_extra.h>
//#include <juce_gui_basics/juce_gui_basics.h>
#include <juce_audio_utils/juce_audio_utils.h>

#include "GUIEuclideanSequence.h"
#include "GUILookAndFeel.h"
#include "Messages.h"

namespace EuclideanRhythms {

////////////////////////////////////////////////////////////////////////////////

class SettingsComponent : public juce::Component
{
 public:
  SettingsComponent(juce::AudioDeviceManager &audioMgr);
  void resized();
  void paint(juce::Graphics &g);

 private:
  juce::TextButton doneButton;
  juce::AudioDeviceSelectorComponent juceSettings;
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SettingsComponent)
};

////////////////////////////////////////////////////////////////////////////////

class GUIMain;

class TopRowComponent : public juce::Component
{
 public:
  TopRowComponent(GUIMain &guiMain);
  void paint(juce::Graphics &g);
  void resized();

 private:
  GUIMain &guiMain;

  juce::ShapeButton playButton;
  juce::ImageButton tempoButton;
  juce::Slider tempoSlider;

  int tempoClickCounter;
  bool playing;
  double tempoClickTimes[2];
  void tempoButtonClicked();
  void play();
  void tempoSliderChanged();

  juce::ShapeButton plusButton;
  juce::ShapeButton minusButton;
  juce::ImageButton settingsButton;

  friend GUIMain;
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TopRowComponent)
};

////////////////////////////////////////////////////////////////////////////////
class GUIMain : public juce::AnimatedAppComponent
{
 public:
  GUIMain(MessageQueue &msgGuiToAudio, MessageQueue &msgAudioToGui,
          juce::AudioDeviceManager &audioManager);
  ~GUIMain();
  void paint(juce::Graphics &g);
  void resized();
  void update();

 private:
  MessageQueue &msgGuiToAudio;
  MessageQueue &msgAudioToGui;
  Message incomingMessage;

  GUILookAndFeel guiLookAndFeel;

  TopRowComponent topRowComponent;
  SettingsComponent settingsComponent;

  const int maxSequences;
  const int initialSequences;
  juce::OwnedArray<GUIEuclideanSequence> sequences;
  juce::Viewport scrollBar;
  juce::Component scrollContainer;
  juce::FlexBox scrollFlexBox;

  void setRhythm(Message &msg);
  void setRhythmLength(Message &msg);
  void setBeat(int b);
  void plusButtonClick();
  void minusButtonClick();

  friend TopRowComponent;
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(GUIMain)
};
} // namespace EuclideanRhythms
