#include "Messages.h"

#include <juce_core/juce_core.h>

namespace EuclideanRhythms {

  unsigned int Message::getUIntValue()
  {
    return value.unsignedInteger;
  }

  int Message::getIntValue()
  {
    return value.integer;
  }

  float Message::getFloatValue()
  {
    return value.floatingPoint;
  }

  /////////////////////////////////////////////////////////////////////////////

  MessageQueue::MessageQueue() : moodycamel::ReaderWriterQueue<Message>(MESSAGE_QUEUE_SIZE) {}

  bool MessageQueue::sendFloat(MessageType type, float value)
  {
    return sendFloat(type, 0, value);
  }

  bool MessageQueue::sendFloat(MessageType type, int index, float value)
  {
    outgoingMessage.type = type;
    outgoingMessage.index = index;
    outgoingMessage.value.floatingPoint = value;
    bool success = try_enqueue(outgoingMessage);
    jassert(success);
    return success;
  }

  bool MessageQueue::sendInt(MessageType type, int value)
  {
    return sendInt(type, 0, value);
  }

  bool MessageQueue::sendInt(MessageType type, int index, int value)
  {
    outgoingMessage.type = type;
    outgoingMessage.index = index;
    outgoingMessage.value.integer = value;
    bool success = try_enqueue(outgoingMessage);
    jassert(success);
    return success;
  }

  bool MessageQueue::sendUInt(MessageType type, unsigned int value)
  {
    return sendUInt(type, 0, value);
  }

  bool MessageQueue::sendUInt(MessageType type, int index, unsigned int value)
  {
    outgoingMessage.type = type;
    outgoingMessage.index = index;
    outgoingMessage.value.unsignedInteger = value;
    bool success = try_enqueue(outgoingMessage);
    jassert(success);
    return success;
  }

  bool MessageQueue::receive(Message &msg)
  {
    bool success = try_dequeue(msg);
    return success;
  }
}
