#pragma once

//#include <juce_graphics/juce_graphics.h>
#include <juce_gui_basics/juce_gui_basics.h>

namespace EuclideanRhythms {
namespace Colours {
const juce::Colour background{0xff222222};
const juce::Colour hard{0xffdddddd};
const juce::Colour soft{0xff555555};
const juce::Colour highlight{0xfffd8612};
} // namespace Colours

namespace Sizes {
const float lineThickness = 1.0;
const float margin = 20.0;
const float halfMargin = margin / 2.0;

const float sliderHeight = 20.0;
const float sliderWidth = 45.0;
const float buttonWidth = 60.0;
const float labelWidth = 95.0;

const float rhythmDiameter = 130.0;
const float rhythmMargin = 40.0;
const float rhythmPadding = 15.0;
const float rhythmStep = 7.0;
const float rhythmControlsWidth = labelWidth + sliderWidth + margin;
const float rhythmWidth = rhythmDiameter + rhythmControlsWidth + 2 * rhythmPadding;
const float rhythmHeight = rhythmDiameter + 2 * rhythmPadding;

const float topRowHeight = sliderHeight + margin;
const float transportWidth = 140.0;
const float numChannelsWidth = 3.0 * sliderHeight + margin;

const float windowWidth = rhythmWidth * 2.0 + 3.0 * margin;
const float windowHeight = rhythmHeight * 2.0 + 2.5 * margin + topRowHeight;

///////////////////////////////////

} // namespace Sizes

class GUILookAndFeel : public juce::LookAndFeel_V4 {
 public:
  GUILookAndFeel();
  ~GUILookAndFeel();

  juce::Slider::SliderLayout getSliderLayout(juce::Slider &);

  void drawButtonBackground(juce::Graphics &g, juce::Button &button,
                            const juce::Colour &backgroundColour,
                            bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown);

  const juce::Font &getLightFont();
  const juce::Font &getMediumFont();
  const juce::Font &getBoldFont();

  juce::Font getLabelFont(juce::Label &label);

 private:
  juce::Font fontLight;
  juce::Font fontMedium;
  juce::Font fontBold;
};
} // namespace EuclideanRhythms
