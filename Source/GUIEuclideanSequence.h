#pragma once

#include <juce_gui_basics/juce_gui_basics.h>

#include "AudioEuclideanSequence.h"
#include "Messages.h"

namespace EuclideanRhythms {

  class GUIEuclideanSequence : public juce::Component {
   public:
    GUIEuclideanSequence(int idx, MessageQueue &msgQueueGuiToAudio);
    ~GUIEuclideanSequence();
    void paint(juce::Graphics &);
    void resized();
    void setBeat(int b);
    void setRhythm(unsigned int r);
    void setRhythmLength(int l);

   private:
    /** @brief A numeric identifier for this object. Also the slot that this object occupies in the
     * containing "sequences" array.  */
    int index;
    MessageQueue &msgGuiToAudio;

    float centerX;
    float centerY;

    int beat;

    juce::Label labelSteps;
    juce::Slider sliderSteps;

    juce::Label labelPulses;
    juce::Slider sliderPulses;

    juce::Label labelRotate;
    juce::Slider sliderRotate;

    juce::Array<juce::Point<float>> squarePositions;
    juce::Array<juce::Point<float>> squareCenterPositions;
    Rhythm rhythm;
    int rhythmLengthTemp;

    void calculatePositions(int steps, float centerX, float centerY,
                            juce::Array<juce::Point<float>> &positions,
                            juce::Array<juce::Point<float>> &centerPositions);

    void drawCircle(juce::Graphics &, float centerX, float centerY, int currentStep);

    void stepsSliderChanged();
    void rotateSliderChanged();
    void pulsesSliderChanged();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(GUIEuclideanSequence)
  };
}
