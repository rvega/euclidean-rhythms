#include "Rhythm.h"

namespace EuclideanRhythms {
  Rhythm::Rhythm()
  {
    // Sanity check. Can we fit 32 bits (4 bytes) in an int?
    jassert(sizeof(int) >= 4);
  }

  Rhythm::~Rhythm() {}

  void Rhythm::toInt(unsigned int &i, int &length)
  {
    i = 0;
    for (int bit = 0; bit < notes.size(); ++bit) {
      bool bitIsOn = notes[bit];
      if (bitIsOn) {
        i += (1 << bit);
      }
    }

    length = notes.size();
  }

  void Rhythm::fromInt(unsigned int integer, int length)
  {
    jassert(length <= MAX_RHYTHM_SIZE);

    notes.clearQuick();
    for (int i = 0; i < length; ++i) {
      notes.add(((integer >> i) & 1));
    }
  }

  // https://www.computermusicdesign.com/simplest-euclidean-rhythm-algorithm-explained/
  void Rhythm::calculate(int steps, int pulses, int offset)
  {
    jassert(steps > 0);
    jassert(steps >= pulses);
    jassert(steps >= offset);

    notes.clearQuick();

    int bucket = 0;
    for (int i = 0; i < steps; ++i) {
      bucket += pulses;
      if (bucket >= steps) {
        bucket -= steps;
        notes.add(true);
      }
      else {
        notes.add(false);
      }
    }

    // Rotate.
    for (int i = 0; i < offset + 1; ++i) {
      bool last = notes[steps - 1];
      notes.removeLast();
      notes.insert(0, last);
    }
  }
}
