
#include <juce_gui_basics/juce_gui_basics.h>

#include "AudioMain.h"
#include "GUILookAndFeel.h"
#include "GUIMain.h"
#include "Messages.h"

namespace EuclideanRhythms {

///////////////////////////////////////////////////////////////////////////////
// Esta clase representa la ventana principal de la aplicación.
// Hereda de Juce::DocumentWindow
class MainWindow : public juce::DocumentWindow {
 public:
  MessageQueue msgGuiToAudio;
  MessageQueue msgAudioToGui;
  AudioMain audioMain;
  GUIMain guiMain;

  MainWindow(juce::String name, juce::Colour color, int buttons) :
      DocumentWindow(name, color, buttons),
      audioMain(msgGuiToAudio, msgAudioToGui),
      guiMain(msgGuiToAudio, msgAudioToGui, audioMain.getAudioManager())
  {
    setContentOwned(&guiMain, true);
    setUsingNativeTitleBar(true);
    centreWithSize(getWidth(), getHeight());
    setVisible(true);
    setResizable(true, true);
    setResizeLimits(Sizes::windowWidth / 2.0, Sizes::windowHeight / 2.0, Sizes::windowWidth * 4.0,
                    Sizes::windowHeight * 4.0);
  }

  void closeButtonPressed()
  {
    // Esta función se ejecuta cuando el usuario cierra la ventana. Aqui,
    // llamamos la función systemRequestedQuit() de la aplicación.
    juce::JUCEApplication::getInstance()->systemRequestedQuit();
  }

 private:
  JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainWindow)
};

///////////////////////////////////////////////////////////////////////////////
// Esta clase representa una aplicación. Note que la clase hereda de
// JUCEApplication.

class App : public juce::JUCEApplication {
 public:
  MainWindow *mainWindow;

  App() {}

  const juce::String getApplicationName()
  {
    return "Euclidean Rythms";
  }

  const juce::String getApplicationVersion()
  {
    return "0.0.1";
  }

  bool moreThanOneInstanceAllowed()
  {
    return true;
  }

  void initialise(const juce::String & /* commandLine */)
  {
    juce::String name = getApplicationName();
    juce::Colour bgColour = Colours::background;
    int buttons = juce::DocumentWindow::allButtons;
    mainWindow = new MainWindow(name, bgColour, buttons);
  }

  void shutdown()
  {
    delete mainWindow;
    mainWindow = nullptr;
  }

  void systemRequestedQuit()
  {
    // Esta función se ejecuta cuando el sistema operativo le pide a la
    // aplicación que se cierre. Es posible ignorar esta petición o llamar la
    // función quit() para efectivamente cerrar.
    quit();
  }

  void anotherInstanceStarted(const juce::String & /* commandLine */)
  {
    // Esta función se ejecuta cuando otra instancia de esta aplicación se
    // inicia. El parámetro commandLine trae los parámetros de línea de comandos
    // que se usaron al iniciar la aplicación desde la terminal.
  }
};

// El macro START_JUCE_APPLICATION genera la función main() de la aplicación.
START_JUCE_APPLICATION(App)

} // namespace EuclideanRhythms
