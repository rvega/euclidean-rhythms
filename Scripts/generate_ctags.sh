#!/bin/sh

# This script reads the source code for the whole project and generates a tags 
# file that can be used by your code editor to navigate the whole code base 
# easily. 

PROJECT_DIR=/home/Rafa/Projects/Active/ProgramacionAudio/Euclidean

trap 'rm -f "$$.tags"' EXIT
rm -f files || true

find $PROJECT_DIR/Source -type f >> files
find $PROJECT_DIR/Vendors/JUCE/modules -type f >> files
find $PROJECT_DIR/Vendors/readerwriterqueue -type f >> files

ctags --tag-relative -L files -f"$$.tags" --languages=c,c++
rm files
mv "$$.tags" "$PROJECT_DIR/tags"
