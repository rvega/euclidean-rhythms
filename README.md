## Dependencies

JUCE (included in this repo as a git subtree). 
cmake
ninja
ctags
gcc

## Configuring

```
cd path/to/this/repo
mkdir Build
cd Build
cmthis/repo
mkdir Build
cd Build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
```

Then, edit the scripts in the Scripts folder so that your paths are correct. You might find those scripts useful.

## Compiling and Running

```
cd path/to/this/repo
cd Build
ninja
./Euclidean
```

